const firstToOpen =  'assets/images/default.png';
let app = angular.module('librasQuim', []);
let player = new Clappr.Player({parentId: "#player", source: firstToOpen});

exports.app = app;
exports.player = player;

const  { app, player } = require('./assets/js/main.js')
const { Menu } = require('./assets/js/menu.js')
const $ = require('jquery')
const fs = require('fs')


class App {
  constructor() {
    Menu.start()
    this.setPlayerSize(player)
  }

  setPlayerSize(player) {
    let container = $(".main-container");

    player.resize({width: container.width(), height: container.height()})
    console.log(container.width)
  }
}

new App()

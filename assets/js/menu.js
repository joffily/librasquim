const  { app, player } = require('./main.js');


class Menu {

  static start() {
    app.controller('MenuController', function MenuController($scope) {
      let path = 'videos/';
      let videos = fs.readdirSync(path);
      $scope.words = [];

      videos = videos.filter(v => {
          return v.search('.mp4') > -1
      })

      videos.forEach(v => {
          let name = v.split('.')[0];
          $scope.words.push({
            name: name,
            link: path + v
          });
      })

      $scope.play = function(e, video) {
        e.preventDefault();
        player.load(video);
        player.play();
      }

    })
  }

}

exports.Menu = Menu;
